package org.opentele.server.provider

import grails.plugin.springsecurity.annotation.Secured
import org.opentele.server.core.model.types.PermissionName
import org.opentele.server.model.Patient
import org.opentele.server.model.TextMessageTemplate
import org.opentele.server.provider.Exception.FailedToSendSmsException
import org.opentele.server.provider.Exception.InvalidSmsIntegrationConfigurationException

@Secured(PermissionName.NONE)
class TextMessagesController {

    def textMessagesService
    def sessionService

    @Secured(PermissionName.TEXT_MESSAGE_SEND)
    def send() {

        if (!params.id) {
            redirectToPatientOverview()
            return
        }

        def templates = TextMessageTemplate.findAll()
        [templates: templates, patientId: params.id]
    }

    @Secured(PermissionName.TEXT_MESSAGE_SEND)
    def submit() {

        def patientId = params.long("patientId")
        if (!patientId) {
            redirectToPatientOverview()
            return
        }

        String messageContent = params.selected
        if (!messageContent) {
            throw new IllegalArgumentException("params.selected must be set")
        }

        def patient = Patient.get(patientId)
        if (!patient) {
            redirectToPatientOverview()
            return
        }

        String messageCode = 'textMessages.message.sent'
        try {
            textMessagesService.send(patient.mobilePhone, messageContent)
        } catch (FailedToSendSmsException ex) {
            messageCode = 'textMessages.message.sent.failed'
        } catch (InvalidSmsIntegrationConfigurationException ex) {
            messageCode = 'textMessages.sms.notconfigured'
        }

        flash.message = message(code: messageCode)
        redirect(action: "send", params: [id: patientId.toString()])
    }

    private def redirectToPatientOverview() {
        log.warn("No patient specified. Redirecting to patient overview")
        sessionService.setNoPatient(session)
        redirect(action: "index", controller: "patientOverview")
    }
}
