<%@ page import="org.opentele.server.model.TextMessageTemplate" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName"
           value="${message(code: 'textMessageTemplate.label', default: 'Standard Text Message')}" />
    <title><g:message code="default.edit" args="[entityName]"/></title>
</head>
<body>
<div id="edit-textMessageTemplate" class="content scaffold-edit" role="main">
    <h1><g:message code="default.edit" args="[entityName]" /></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${template}">
        <ul class="errors" role="alert">
            <g:eachError bean="${template}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form method="post" >
        <g:hiddenField name="id" value="${template?.id}" />
        <g:hiddenField name="version" value="${template?.version}" />
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
        <fieldset class="buttons">
            <g:actionSubmit class="save" action="update" value="${message(code: 'default.update', default: 'Update')}" />
            <g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate=""
                            onclick="return confirm('${message(code: 'textMessageTemplate.delete.message.confirm')}');" />
        </fieldset>
    </g:form>
</div>
</body>
</html>
