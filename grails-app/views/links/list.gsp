
<%@ page import="org.opentele.server.model.LinksCategory" %>
<!doctype html>
<html>
<head>
    <meta name="layout" content="main">
    <title><g:message code="linksCategory.list.label" /></title>
</head>
<body>
<div id="list-links" class="content scaffold-list" role="main">
    <h1><g:message code="linksCategory.list.label"/></h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <table>
        <thead>
        <tr>

            <g:sortableColumn property="name" title="${message(code: 'linksCategory.name.label')}" />
            <th><g:message code="linksCategory.patientgroup.heading.label"/></th>

        </tr>
        </thead>
        <tbody>
        <g:each in="${categories}" status="i" var="category">
            <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                <td><g:link action="show" id="${category.id}">${category.name}</g:link></td>
                <td>${category.patientGroupNames}</td>

            </tr>
        </g:each>
        </tbody>
    </table>
    <div class="pagination">
        <g:paginate total="${categoriesTotal}" />
    </div>

    <fieldset class="buttons">
        <g:link class="create" action="create">
            <g:message code="default.create.label" args="[g.message(code:'linksCategory.label')]" />
        </g:link>
    </fieldset>
</div>
</body>
</html>
